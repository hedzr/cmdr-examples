/*
 * Copyright © 2019 Hedzr Yeh.
 */

package cmdr_examples

const (
	// AppName const
	AppName = "cmdr-examples"
	// Version const
	Version = "1.10.10"
	// VersionInt const
	VersionInt = 0x010a0a
)
